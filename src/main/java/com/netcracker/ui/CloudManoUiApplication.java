package com.netcracker.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class CloudManoUiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudManoUiApplication.class, args);
    }

}
