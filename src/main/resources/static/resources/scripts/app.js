angular.module('mano-cloud-ui', ["ngRoute", "ngAnimate"])
    .config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                template : "<dir-controls-panel></dir-controls-panel><dir-search-section></dir-search-section>"
            })
            .when("/catalog", {
                template : "<dir-controls-panel></dir-controls-panel><dir-search-section></dir-search-section>"
            })
            .when("/design", {
                template : "<dir-service-design class='service_design'></dir-service-design>"
            })
            .otherwise({
                template : "<h1>Nothing to show</h1>"
            });
        });

angular.module('mano-cloud-ui')
    .controller('CloudUIController', ['$scope', '$location', function($scope, $location) {
        $scope.goTo = function(path) {
          $location.path(path);
        };
    }]);