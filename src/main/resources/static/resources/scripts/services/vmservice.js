angular.module('mano-cloud-ui')
    .factory('VmService', ['$http', '$q', function VmServiceFactory($http, $q, $location) {
        return {

            getAllVms: function () {
                let deferred = $q.defer();
                // $http.get('resources/responsetemplate2.json').success(
                $http.get('/vnfs/vnf-package/all').success(
                    function (response) {
                        deferred.resolve(response);
                    }
                ).error(function (error) {
                    console.log('error');
                    deferred.reject(error);
                });
                return deferred.promise;
            }
        }
    }]);