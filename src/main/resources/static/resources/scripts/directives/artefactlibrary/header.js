angular.module('mano-cloud-ui')
    .directive('dirHeader', ['UserService', function (UserService) {
        return {
            templateUrl: "resources/templates/artefactlibrary/header.html",
            replace: true,
            restrict: 'AEC',
            controller: function ($scope) {
                let userData = UserService.getCurrentUser();
                $scope.info = userData.info;
                $scope.name = userData.name;
            }
        };
    }]);