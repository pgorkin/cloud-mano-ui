angular.module('mano-cloud-ui')
    .directive('angCheckBox',function(){
        return {
            restrict: 'E',
            transclude: true,
            template: "<label class=\"checkbox _checked\" ng-click=\"check($event)\" ng-transclude></label>"
        }
    })
    .directive('emitLastRepeaterElement', function() {
        return function(scope) {
            if (scope.$last){
                scope.$emit('LastRepeaterElement');
            }
        };
    })
    .directive('dirSearchSection', ['VmService', function (VmService) {
        return {
            templateUrl: "resources/templates/artefactlibrary/searchsection.html",
            replace : true,
            restrict: 'E',
            controller: function ($scope, $rootScope) {

                VmService.getAllVms().then(
                    function (respData) {
                        $rootScope.data = responseDataConverter(respData);
                    },
                    function (err) {
                        alert(err);
                    }
                );

                //    controls
                /*$('.checkbox ').on('click', function() {
                    $(this).toggleClass('_checked');
                });*/

                $scope.check = function ($event){
                    var elem = $($event.currentTarget);
                    elem.toggleClass(constants._checked);
                }

                /*$('.switcher').on('click', function() {
                    $(this).toggleClass('_active');
                });*/

                $scope.switcher = function ($event){
                    var elem = $($event.currentTarget);
                    elem.toggleClass(constants._active);
                }

            //    search Section
                var constants = {
                    select: "select",
                    selected: "selected",
                    _opened: "_opened",
                    _select: "_select",
                    _selected: "_selected",
                    _active: "_active",
                    _checked: "_checked"
                };

                var filterClasses= {
                    type: ".line-type",
                    vendor: ".line-vendor",
                    groupStatus: ".group-line-status"
                }

                var updateFrame = function () {
                    $searchSection = $('#searchSection');
                    $tabContentLines = $searchSection.find('.tab-content__line');
                    $tabAsideViewAll = $searchSection.find('.tab-aside__view-all');
                    $tabsTabNumber = $searchSection.find('.tabs__tab__number');
                    $buttonStartDesignNumber = $('.button._start-design').find('.button__number');
                    $selectableButtons = $searchSection.find('.jsSelectableButton');
                    $initSelectedButtonsNumber = $initSelectedButtonsNumber/*$selectableButtons.filter(function() {
                        return $(this).hasClass('_selected');
                    }).length*/;
                };

                var $searchSection, $tabContentLines, $tabAsideViewAll, $tabsTabNumber,$buttonStartDesignNumber, $selectableButtons, $initSelectedButtonsNumber = 0;
                updateFrame();

                $searchSection.on('click', '.tabs__tab', function() {
                    var $this = $(this),
                        $linesWithoutSelectedButtons = $tabContentLines
                            .filter(function() {
                                return !$(this).find('.jsSelectableButton' + '.' + constants._selected).length;
                            });
                    $groups=$('.tab-category-block');
                    if ($this.hasClass('jsTabSelected')) {
                        $linesWithoutSelectedButtons.hide();
                        hideEmptyGroups($groups);
                    } else {
                        for(var i=0;i<$groups.length;i++){
                            $group=$groups[i];
                            $($group).show();
                        }
                        $linesWithoutSelectedButtons.show();
                    }
                });

                $scope.changeTab = function($event){
                    var elem = $($event.currentTarget);
                    elem.siblings()
                         .removeClass(constants._active)
                         .end()
                         .addClass(constants._active);
                }

                $tabAsideViewAll.on('click', function() {

                    var $this = $(this),
                        $text = $this.find('.tab-aside__view-all-text');

                    $this.toggleClass(constants._opened);

                    $this
                        .siblings('.tab-aside__row-hidden-wrapper')
                        .slideToggle(300);

                    if ($this.hasClass(constants._opened)) {
                        $text.text('Close');
                    } else {
                        $text.text('View all');
                    }

                });

                $scope.$on('LastRepeaterElement', function(){
                    updateFrame();
                    setText([$tabsTabNumber, $buttonStartDesignNumber], $initSelectedButtonsNumber);
                });

                $scope.lineClicked = function ($event, line) {
                    var elem = $($event.currentTarget);
                    if(elem.hasClass(constants._select)){
                        line.selected = true;
                        elem.text(constants.selected);
                        ++$initSelectedButtonsNumber;
                        setText([$tabsTabNumber, $buttonStartDesignNumber], $initSelectedButtonsNumber);
                    }else{
                        line.selected = false;
                        elem.text(constants.select);
                        --$initSelectedButtonsNumber;
                        setText([$tabsTabNumber, $buttonStartDesignNumber], $initSelectedButtonsNumber);
                        $groups=$('.tab-category-block');
                        $selectedTab=$('.jsTabSelected');
                        if($selectedTab.hasClass(constants._active)){
                            hideEmptyGroups($groups);
                        }
                    }
                };


                /*Helpers*/
                $scope.filterForLines = function (line){
                    var isSelectedByType=checkElement(filterClasses.type,line.type);
                    var isSelectedByVendor=checkElement(filterClasses.vendor,line.vendor);
                    var isMultiTenant=checkMultiTenant(line);
                    var isContainsInName=checkContainsInName(line.name,$scope.$parent.nameSearchPhrase);
                    $selectedTab=$('.jsTabSelected');
                    if($selectedTab.hasClass(constants._active)){
                        if(line.selected){
                            return true;
                        }
                        return false;
                    }
                    return isSelectedByType && isSelectedByVendor && isContainsInName && isMultiTenant;
                }

                $scope.clearInput = function(){
                    $scope.$parent.nameSearchPhrase="";
                }

                $scope.filterForLineGroups = function (category){
                    if($selectedTab.hasClass(constants._active))
                        return true;
                    return checkElement(filterClasses.groupStatus,category.name);
                }

                function checkElement(classOfFilter, parameter){
                     var types=$(classOfFilter).find('.tab-aside__control');
                         for(var i=0; i<types.length; i++){
                             var typeCheckbox=$(types[i]).find('.checkbox');
                                 if($(typeCheckbox).hasClass(constants._checked)){
                                     var name=$(typeCheckbox).find('.checkbox__text').html();
                                     if(parameter==name){
                                         return true;
                                     }
                                 }
                             }
                     return false;
                }

                function checkMultiTenant(line){
                    var switcher=$('.switcher');
                    if(!switcher.hasClass(constants._active)){
                        if(line.multiTenant){
                            return false;
                        }
                        return true;
                    }
                    else{
                        if(!line.multiTenant){
                            return false;
                        }
                        return true;
                    }
                }

                function checkContainsInName(name, substr) {
                    if (!substr || substr.length === 0) {
                        return true;
                    }

                    return name.toLowerCase().indexOf(substr.toLowerCase()) !== -1;
                }

                function hideEmptyGroups($groups){
                     for(var i=0;i<$groups.length;i++){
                        $group=$groups[i];
                        $countSelectedElements=$($group).find('.jsSelectableButton'+'.'+constants._selected).length;
                        if($countSelectedElements==0){
                            $($group).hide();
                        }
                    };
                }

                function setText(arr, text) {
                    arr.forEach(function(item) {
                        if(item!=null) item.text(text);
                    });
                }

                function responseDataConverter(respData) {
                    let data = {
                        "categories": []
                    };

                    function findCategory(arr, status) {
                        let result = $.grep(arr, function(e){ return e.name == status; });
                        if(result.length == 0) return null;
                        return result[0];
                    }

                    angular.forEach(respData, function(value, key){
                        let status = statusTransform(value.status);
                        if(findCategory(data.categories, status)==null){
                            data.categories.push({
                                "name" : status,
                                "data" : []});
                        }
                        findCategory(data.categories, status).data.push(value);
                    });

                    data.categories.sort(function (a, b) {
                        if (a.name > b.name) {
                            return 1;
                        }
                        if (a.name < b.name) {
                            return -1;
                        }
                        return 0;
                    });

                    return data;
                }

                function statusTransform(status) {
                    if(status=="VALID_NOT_TESTED") return "Ready for Test";
                    return status;
                }
            }
        };
    }]);