angular.module('mano-cloud-ui')
.directive('dirServiceDesign', function () {
    return {
        templateUrl: "resources/templates/artefactlibrary/servicedesign.html",
        replace: true,
        restrict: 'E',
        controller: function ($scope, $rootScope) {
            var graphControl;

            function getNodesInfo() {
                if (!$scope.data) {
                    return null;
                }

                var vmf_categories = $scope.data.categories;

                if (!vmf_categories) {
                    return null;
                }

                var nodes = [];
                angular.forEach(vmf_categories, function(category) {
                    if (category && category.data) {
                        angular.forEach(category.data, function(vmf) {
                            if (vmf.selected) {
                                nodes.push(vmf);
                            }
                        });
                    }
                });

                if (nodes.length <= 0) {
                    return null;
                }

                return nodes;
            }

            function getNodeStyle() {
                var nodeStyle = new yfiles.drawing.GeneralPathNodeStyle();
                nodeStyle.drawShadow = true;
                return nodeStyle;
            }

            function getInputMode() {
                var inputMode = new yfiles.input.GraphEditorInputMode();
                inputMode.nodeCreationAllowed = false;
                inputMode.edgeCreationAllowed = true;
                return inputMode;
            }

            function drawGraph(nodesInfo) {
                if (!nodesInfo) {
                    return;
                }

                var nodeStyle = getNodeStyle();
                graphControl.graph.nodeDefaults.style = nodeStyle;

                var graph = graphControl.graph;

                angular.forEach(nodesInfo, function(nodeInfo) {
                    var currentNode = graph.createNodeWithBoundsAndStyle(new yfiles.geometry.RectD(10, 10, 100, 100), nodeStyle);
                    graph.addLabel(currentNode, nodeInfo.name);
                });

                graphControl.inputMode = getInputMode();
            }

            require([
                'resources/scripts/libs/thirdparty/com.yworks.yfiles/yfiles-html/1.3.0.5/license.js',
                'resources/scripts/libs/thirdparty/com.yworks.yfiles/yfiles-html/1.3.0.5/complete'
            ],
            function() {
                if (!graphControl) {
                    graphControl = new yfiles.canvas.GraphControl.ForId('graphCanvas');
                }

                drawGraph(getNodesInfo());
            });
        }
    };
});