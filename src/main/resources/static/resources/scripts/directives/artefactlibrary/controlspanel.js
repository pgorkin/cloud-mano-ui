angular.module('mano-cloud-ui')
    .directive('dirControlsPanel', ['$location', function ($location) {
        return {
            templateUrl: "resources/templates/artefactlibrary/controlspanel.html",
            replace: true,
            restrict: 'E',
            controller: function ($scope) {

                var constants = {
                };



                var $searchInput = $('#searchInput'),
                    $searchInputButton = $searchInput.find('.search-input__button'),
                    $searchInputInput = $searchInput.find('.input'),
                    $searchInputInner = $searchInput.find('.search-input__inner'),
                    $searchSection = $('#searchSection');

                $scope.$on('LastRepeaterElement', function(){
                    $searchSection = $('#searchSection');
                });

                var $startDesignButton =  $('#controlsPanel').find('.button._start-design');

                $searchInputButton.on('click', function() {
                    var $this = $(this);

                    if ($searchInput.hasClass('_opened')) {

                        $searchInputInner
                            .velocity({
                                translateX: '-100%'
                            }, {
                                easing: "ease-out",
                                duration: 200
                            });

                        $searchInputButton
                            .velocity({
                                rotateZ: 0
                            })
                            .velocity({
                                rotateX: -90
                            }, {
                                duration: 200,
                                complete: function() {

                                    $searchInput
                                        .removeClass('_opened')
                                        .addClass('_closed');

                                }
                            }).velocity({
                            rotateX: 0
                        }, {
                            duration: 200
                        });

                        $searchSection.velocity({
                            translateY: '10%',
                            top: '300px',
                            opacity: 0
                        }, {
                            duration: 300,
                            easing: "ease-out",
                            display: "none"
                        });
                         $startDesignButton
                             .velocity({
                                  translateX: '100%'
                             }, {
                                  easing: "ease-out",
                                  duration: 200,
                                  complete: function(){
                                       $startDesignButton[0].style.display = 'none';
                                  }
                             });
                    } else if ($searchInput.hasClass('_closed')) {

                        $searchInputButton
                            .velocity({
                                rotateX: -90
                            }, {
                                duration: 200,
                                complete: function() {
                                    $searchInput
                                        .addClass('_opened')
                                        .removeClass('_closed');
                                },
                                begin: function() {
                                    $searchInputInner
                                        .velocity({
                                                translateX: '-100%'
                                            },
                                            {
                                                duration: 0
                                            });
                                }
                            }).velocity({
                            rotateX: 0
                        }, {
                            duration: 200,
                            complete: function() {
                                $searchInputInner
                                    .velocity({
                                        translateX: 0
                                    }, {
                                        easing: "ease-in",
                                        duration: 200,
                                        complete: function() {
                                            $searchInputInput.focus();
                                        }
                                    });
                            }
                        })
                            .velocity({
                                rotateZ: 180
                            });

                        $searchSection.velocity({
                            translateY: 0,
                            top: '90px',
                            opacity: 1
                        }, {
                            duration: 300,
                            easing: "ease-out",
                            display: "block"
                        });
                        $startDesignButton[0].style.display = 'block';
                        $startDesignButton
                            .velocity({
                                translateX: 0
                            }, {
                                easing: "ease-in",
                                duration: 500});
                    }
                })

            }
        };
    }]);