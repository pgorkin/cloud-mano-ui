module.exports = {
    autoprefixer: {
        browsers: [
            'last 2 versions',
            'safari 5',
            'ie 9',
            'opera 12.1',
            'ios 6',
            'android 4'
        ],
        cascade: true
    },

    // LESS config
    less: {
        src: './styles/*.less',
        dest: './styles'
    },

    // Icons config
    icons: {
        src: './images/icons/*',
        dest: './styles/common',
        template: './gulp/icons-template',
        concat: 'icons.less'
    },

    // Browser Sync config
    bsync: {
        base: './',
        start: './markup/'
    },

    // Watch config
    watch: {
        html: 'markup/**/*.html',
        less: 'styles/**/*.less',
        icons: 'images/icons/*',
        mocks: 'scripts/mocks/**/*.js'
    },

    // Plugins config
    plugins: {
        scope: ['dependencies', 'devDependencies', 'peerDependencies'],
        rename: {
            'gulp-sourcemaps': 'sourcemaps',
            'gulp-plumber': 'plumber',
            'gulp-less': 'less',
            'gulp-image-data-uri': 'uri',
            'gulp-concat': 'concat',
            'gulp-ignore': 'ignore'
        }
    }
};