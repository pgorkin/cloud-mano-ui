FROM docker.netcracker.com:17008/openjdk:8-alpine
MAINTAINER Pavel Gorkin <pavel.gorkin@netcracker.com>
ADD target/cloud-mano-ui-1.0-SNAPSHOT.jar /app/app.jar
EXPOSE 9090

ENTRYPOINT ["java", "-jar", "/app/app.jar"]
