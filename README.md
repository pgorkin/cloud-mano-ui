#### Build

    1. mvn clean package
    2. docker build -t docker.netcracker.com:17008/cloud-mano-ui .
    3. docker login docker.netcracker.com:17008
    4. docker push docker.netcracker.com:17008/cloud-mano-ui

#### Run

###### Locally
    
    docker-compose up